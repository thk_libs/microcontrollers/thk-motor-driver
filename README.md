# **DC-Motor Driver**

Eine Klasse zur Ansteuerung von Gleichstrommotoren für unterschiedliche Motortreiber.

Getestet mit:

- Arduino Mega Pro + TB6612FNG<br />
<br />

## **Installation:**

Um diese Klassen verwenden zu können, muss dieses Repository geklont und in das Library-Verzeichnis der Arduino-IDE kopiert werden.<br />
<br />

## **Anwendung:**

Zur Verwendung siehe zunächst das Beispiel `drive.ino`<br />
<br />

**Erläuterung zur Klasse**

Es wird für jeden Motortreiber nur ein Objekt benötigt, dem beim instanziieren alle benötigten Informationen übergeben wird.<br />
<br />

**Einbinden der Libraries:**

```arduino
#include <thk_motor_driver_tb6612fng.h>
```

**Instanziieren eines Motortreiber-Objektes:**

```arduino
thk_MotorDriverTB6612FNG motorExample(MOTOR_COUNT, MOTOR_STBY, MOTOR_PWM_A, MOTOR_IN1, MOTOR_IN2, MOTOR_PWM_B, MOTOR_IN3, MOTOR_IN4)
```

 Die Parameter des Motortreiber-Objektes:

- `MOTOR_COUNT`: Anzahl der anzusteuernden Gleichstrommotoren
- `MOTOR_STBY`: Pin für die Steuerung des Motortreiber Chips.
- `MOTOR_PWM_A`: PWM Pin für die Steuerung der Motorgeschwindigkeit vom Gleichstrommotor A.
- `MOTOR_IN1`: Input Pin 1 vom Gleichstrommotor A.
- `MOTOR_IN2`: Input Pin 2 vom Gleichstrommotor A.
- `MOTOR_PWM_B`: PWM Pin für die Steuerung der Motorgeschwindigkeit vom Gleichstrommotor B.
- `MOTOR_IN3`: Input Pin 3 vom Gleichstrommotor B.
- `MOTOR_IN4`: Input Pin 4 vom Gleichstrommotor B.

**Funktionen:**

```arduino
motorExample.drive(velocity, direction);    // Fahren mit definierter Geschwindigkeit (0-255) und mit Angabe der Richtung. 1 = Vorwärts, 0 = Rückwärts
motorExample.drive_forward(velocity);       // Vorwärtsfahren mit definierter Geschwindigkeit (0-255)
motorExample.drive_backward(velocity);      // Rückwärtsfahren mit definierter Geschwindigkeit (0-255)
motorExample.stop();                        // Stoppen des Motors
```
