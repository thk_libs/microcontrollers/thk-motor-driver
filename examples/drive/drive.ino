#include <thk_motor_driver_tb6612fng.h>

uint8_t velocity = 255;

const int MOTOR_COUNT = 1;
const int MOTOR_STBY = 38;
const int MOTOR_PWM_A = 6;
const int MOTOR_IN1 = 40;
const int MOTOR_IN2 = 42;
const int MOTOR_PWM_B = 0;
const int MOTOR_IN3 = 0;
const int MOTOR_IN4 = 0;
thk_MotorDriverTB6612FNG motor(MOTOR_COUNT, MOTOR_STBY, MOTOR_PWM_A, MOTOR_IN1, MOTOR_IN2, MOTOR_PWM_B, MOTOR_IN3, MOTOR_IN4);

void setup()
{
    Serial.begin(115200);
    motor.init();
}

void loop()
{
    motor.drive_forward(velocity);
    delay(2000);

    motor.stop();
    delay(1000);

    motor.drive_backward(velocity);
    delay(2000);
    
    motor.stop();
    delay(1000);

    motor.drive(velocity, 1);
    delay(2000);

    motor.stop();
    delay(1000);

    motor.drive(velocity, 0);
    delay(2000);
    
    motor.stop();
    delay(1000);
}